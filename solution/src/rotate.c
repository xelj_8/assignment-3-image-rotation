#include "../include/rotate.h"
#include "../include/status.h"
#include <stdlib.h>

void rotate_on_90( struct image const source, struct image *new_image, const uint_fast32_t width, const uint_fast32_t height )
{
  new_image->width = height;
  new_image->height = width;
  for (uint_fast32_t row = 0; row < height; row++)
    for (uint_fast32_t column = 0; column < width; column++)
      new_image->data[get_data_index(height, width-column-1, row)] = source.data[get_data_index(width, row, column)];
}

void rotate_on_180( struct image const source, struct image *new_image, const uint_fast32_t width, const uint_fast32_t height )
{
  new_image->width = width;
  new_image->height = height;
  for (uint_fast32_t row = 0; row < height; row++)
    for (uint_fast32_t column = 0; column < width; column++)
      new_image->data[get_data_index(width, height-row-1, width-column-1)] = source.data[get_data_index(width, row, column)];
}

void rotate_on_270( struct image const source, struct image *new_image, const uint_fast32_t width, const uint_fast32_t height )
{
  new_image->width = height;
  new_image->height = width;
  for (uint_fast32_t row = 0; row < height; row++)
    for (uint_fast32_t column = 0; column < width; column++)
      new_image->data[get_data_index(height, column, height-row-1)] = source.data[get_data_index(width, row, column)];
}

static struct image rotate( struct image source, void(*rotate_function)(struct image const, struct image *, const uint_fast32_t, const uint_fast32_t) )
{
  struct image new_image;
  const uint_fast32_t width = source.width;
  const uint_fast32_t height = source.height;

  new_image.data = malloc(width * height * sizeof(struct pixel));
  if (new_image.data == NULL)
  {
    new_image.width = width;
    new_image.height = height;
    return new_image;
  }

  rotate_function(source, &new_image, width, height);

  image_free(&source);
  return new_image;
}

struct image rotate_on_angle( struct image img, angle_of_rotation angle )
{
  if (angle == ANGLE_90)  img = rotate(img, rotate_on_90);
  else if (angle == ANGLE_180)  img = rotate(img, rotate_on_180);
  else if (angle == ANGLE_270)  img = rotate(img, rotate_on_270);

  return img;
}
