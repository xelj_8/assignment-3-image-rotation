#include "../include/validate.h"
#include "../include/utils.h"
#include <stdlib.h>

valid_status validate_cmd_args( int const count, char** const args,
  char** const source_img, char** const target_img, angle_of_rotation* const angle )
{
  if (!is_valid_count_args(count))
    return INVALID_NUMBER_OF_OPERANDS;
  
  *source_img = args[1];
  *target_img = args[2];

  if (!validate_angle(args[3], angle))
    return INVALID_ANGLE;

  return VALID;
}

bool is_valid_count_args( int const count )
{
  return count == 4;
}

bool validate_angle( char* const angle, angle_of_rotation* const result_angle )
{
  char* p;
  int_fast16_t raw_angle;

  if (!is_number(angle))
    return false;
  
  p = angle;
  while (*p)
    p++;
  if (p - angle > 5)
    return false;

  raw_angle = (int_fast16_t) strtol(angle, &p, 10);
  raw_angle = make_geometric_angle(raw_angle);
  if (modulo_90(raw_angle) != 0)
    return false;
  
  *result_angle = raw_angle / 90;
  return true;
}
