#include "validate.h"
#include "bmpio.h"
#include "rotate.h"
#include "utils.h"
#include <stdlib.h>

int main( int argc, char** argv ) {
  char* source_image;
  char* target_image;
  angle_of_rotation angle;
  int result_status;
  struct image img;

  // Validation
  result_status = validate_cmd_args(argc, argv, &source_image, &target_image, &angle);
  if (result_status != VALID)
  {
    print_error("Validation", result_status);
    return result_status;
  }

  // Reading
  result_status = read_image_bmp(source_image, &img);
  if (result_status != READ_OK)
  {
    image_free(&img);
    print_error("Reading", result_status);
    return result_status;
  }

  // Rotation
  img = rotate_on_angle(img, angle);

  // Writing
  result_status = write_image_bmp(target_image, &img);
  if (result_status != READ_OK)
  {
    image_free(&img);
    print_error("Writing", result_status);
    return result_status;
  }

  // Freeing up resources
  print_message("Freeing up resources");
  image_free(&img);
  print_message("OK");
  return result_status;
}
