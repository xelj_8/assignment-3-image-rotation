#include "../include/bmpio.h"
#include "../include/bmp.h"
#include <stdio.h>

read_status read_image_bmp( char* const filename, struct image* img )
{
  FILE* file;
  read_status result_status;

  if (!filename)
    return READ_INVALID_FILENAME;

  file = fopen(filename, "rb");
  if (!file)
    return READ_INVALID_FILENAME;

  result_status = from_bmp(file, img);

  fclose(file);
  return result_status;
}

write_status write_image_bmp( char* const filename, struct image* const img )
{
  FILE* file;
  write_status result_status;

  if (!filename)
    return WRITE_INVALID_FILENAME;

  file = fopen(filename, "wb");
  if (!file)
    return WRITE_INVALID_FILENAME;

  result_status = to_bmp(file, img);

  if (fclose(file))
    return WRITE_INVALID_FILENAME;

  return result_status;
}
