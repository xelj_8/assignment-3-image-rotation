#include "../include/bmp.h"
#include "../include/utils.h"
#include <stdlib.h>

#define BF_TYPE 19778
#define B_OFF_BITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define PELS_PER_METER 0

read_status from_bmp( FILE* source, struct image* img )
{
  char* table;
  struct bmp_header header;

  if (fread(&header, sizeof(struct bmp_header), 1, source) != 1)
    return READ_INVALID_HEADER;

  if (header.biBitCount != BI_BIT_COUNT || header.bfType != BF_TYPE)
    return READ_INVALID_HEADER;

  table = malloc(header.biSizeImage);
  if (table == NULL)
    return READ_OUT_OF_MEMORY;

  if (fread(table, header.biSizeImage, 1, source) != 1)
  {
    free(table);
    return READ_INVALID_DATA;
  }
  
  *img = image_create(header.biWidth, header.biHeight, table);

  free(table);
  return READ_OK;
}

static void standard_header_init(struct bmp_header* const header, struct image const* img)
{
  uint_fast32_t img_size;
  
  img_size = get_image_size(*img);
  header->bfType = BF_TYPE;
  header->bfileSize = img_size + B_OFF_BITS;
  header->bfReserved = 0;
  header->bOffBits = B_OFF_BITS;
  header->biSize = BI_SIZE;
  header->biWidth = img->width;
  header->biHeight = img->height;
  header->biPlanes = BI_PLANES;
  header->biBitCount = BI_BIT_COUNT;
  header->biCompression = 0;
  header->biSizeImage = img_size;
  header->biXPelsPerMeter = PELS_PER_METER;
  header->biYPelsPerMeter = PELS_PER_METER;
  header->biClrUsed = 0;
  header->biClrImportant = 0;
}

write_status to_bmp( FILE* target, struct image const* img )
{
  char* table;
  struct bmp_header header;
  const uint_fast32_t img_size = get_image_size(*img);

  standard_header_init(&header, img);

  if (fwrite(&header, sizeof(struct bmp_header), 1, target) != 1)
    return WRITE_INVALID_HEADER;
  
  table = pixels_to_chars(*img);
  if (fwrite(table, 1, img_size, target) != img_size)
  {
    free(table);
    return WRITE_INVALID_DATA;
  }

  free(table);
  return WRITE_OK;
}
