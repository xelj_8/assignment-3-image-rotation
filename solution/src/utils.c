#include "../include/utils.h"
#include <stdio.h>
#include <string.h>

uint_fast32_t round_4x( uint_fast32_t number )
{
  if (number % 4 == 0)
    return number;
  else
    return number + 4 - number % 4;
}

int_fast16_t make_geometric_angle( int_fast16_t angle )
{
  while (angle < 0)
    angle += 360;
  if (angle >= 360)
    angle %= 360;
  
  return angle;
}

int_fast16_t modulo_90( int_fast16_t number )
{
  return number % 90;
}

bool is_number( char* const string )
{
  if (!string)
    return false;

  if (string[0] != '-' && (string[0] > '9' || string[0] < '0'))
    return false;

  for (size_t i = 1; i < strlen(string); i++) {
    if (string[i] > '9' || string[i] < '0')
      return false;

  }
  return true;
}

void print_error( char* const stage, int const status )
{
  fprintf(stderr, "%s failed with code: %i\n", stage, status);
}

void print_message( char* const message )
{
  printf("%s\n",message);
}
