#include "../include/image.h"
#include "../include/utils.h"
#include <stdlib.h>

struct pixel pixel_create( char* const table )
{
  return (struct pixel) {table[0], table[1], table[2]};
}

static uint_fast32_t extra_pxls_in_row( uint_fast32_t width ) {
  return round_4x(width * sizeof(struct pixel)) - width * sizeof(struct pixel);
}

struct image image_create( uint_fast32_t width, uint_fast32_t height, char* table )
{
  uint_fast32_t extra_pxls = extra_pxls_in_row(width);
  struct pixel* pixels = malloc(width * height * sizeof(struct pixel));
  struct image img = (struct image) {width, height, pixels};

  if (pixels == NULL)
    return img;
  
  for (uint_fast32_t i = 0; i < height; i++, table += extra_pxls)
    for (uint_fast32_t j = 0; j < width; j++, table += sizeof(struct pixel))
      pixels[get_data_index(width, i, j)] = pixel_create(table);

  return img;
}

char* pixels_to_chars( struct image img )
{
  uint_fast32_t extra_pxls = extra_pxls_in_row(img.width);
  char* const chars = malloc(get_image_size(img));
  const uint_fast32_t row_length = round_4x(img.width * sizeof(struct pixel));
  const uint_fast32_t width = img.width;
  const struct pixel* data = img.data;

  if (chars == NULL)
    return chars;

  for (uint_fast32_t i = 0; i < img.height; i++)
  {
    for (uint_fast32_t j = 0; j < width; j++)
      char_set_pixel(&chars[i*row_length + j*sizeof(struct pixel)], data[i*width + j]);
    for (uint_fast32_t j = 0; j < extra_pxls; j++)
      chars[i * row_length + width * sizeof(struct pixel) + j] = 0;
  }

  return chars;
}

uint_fast32_t get_data_index( uint_fast32_t width, uint_fast32_t row, uint_fast32_t column )
{
  return row * width + column;
}

uint_fast32_t get_image_size( struct image img )
{
  return round_4x(img.width * sizeof(struct pixel)) * img.height;
}

void char_set_pixel(char* const chars, const struct pixel data)
{
  *(chars) = (char) data.r;
  *(chars + 1) = (char) data.g;
  *(chars + 2) = (char) data.b;
}

void image_free( struct image *img )
{
  free(img->data);
}
