#include <stdint.h>

#ifndef IMAGE_H
#define IMAGE_H

#pragma pack(push, 1)
struct pixel
{
  uint_fast8_t r, g, b;
};
#pragma pack(pop)

struct image
{
  uint_fast32_t width, height;
  struct pixel* data;
};

struct pixel pixel_create( char* const table );

struct image image_create( uint_fast32_t width, uint_fast32_t height, char* table );

char* pixels_to_chars( struct image img );

uint_fast32_t get_data_index( uint_fast32_t width, uint_fast32_t row, uint_fast32_t column );

uint_fast32_t get_image_size( struct image img );

void char_set_pixel(char* const chars, const struct pixel data);

void image_free( struct image *img );

#endif /* IMAGE_H */
