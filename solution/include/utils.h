#include <stdbool.h>
#include <stdint.h>

#ifndef UTILS_H
#define UTILS_H

uint_fast32_t round_4x( uint_fast32_t number );

int_fast16_t make_geometric_angle( int_fast16_t angle );

int_fast16_t modulo_90( int_fast16_t number );

bool is_number( char* const string );

void print_error( char* const stage, int const status );

void print_message( char* const message );

#endif /* UTILS_H */
