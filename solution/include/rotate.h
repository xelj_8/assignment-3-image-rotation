#include "image.h"
#include <stdbool.h>

#ifndef ROTATE_H
#define ROTATE_H

typedef enum
{
  ANGLE_0 = 0,
  ANGLE_90,
  ANGLE_180,
  ANGLE_270
} angle_of_rotation;

struct image rotate_on_angle( struct image img, angle_of_rotation angle );

void rotate_on_90( struct image const source, struct image *new_image, const uint_fast32_t width, const uint_fast32_t height );

void rotate_on_180( struct image const source, struct image *new_image, const uint_fast32_t width, const uint_fast32_t height );

void rotate_on_270( struct image const source, struct image *new_image, const uint_fast32_t width, const uint_fast32_t height );

#endif /* ROTATE_H */
