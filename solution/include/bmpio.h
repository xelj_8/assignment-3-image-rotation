#include "image.h"
#include "status.h"

#ifndef BMPIO_H
#define BMPIO_H

read_status read_image_bmp( char* const filename, struct image* img );

write_status write_image_bmp( char* const filename, struct image* const img );

#endif /* BMPIO_H */
