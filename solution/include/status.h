#ifndef STATUS_H
#define STATUS_H

typedef enum
{
  READ_OK = 0,
  READ_INVALID_FILENAME,
  READ_INVALID_HEADER,
  READ_INVALID_DATA,
  READ_OUT_OF_MEMORY
} read_status;

typedef enum
{
  WRITE_OK = 0,
  WRITE_INVALID_FILENAME,
  WRITE_INVALID_HEADER,
  WRITE_INVALID_DATA,
  WRITE_OUT_OF_MEMORY
} write_status;

typedef enum
{
  VALID = 0,
  INVALID_NUMBER_OF_OPERANDS,
  INVALID_ANGLE
} valid_status;

#endif /* STATUS_H */
