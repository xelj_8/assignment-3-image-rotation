#include "rotate.h"
#include "status.h"
#include <stdint.h>

#ifndef VALIDATE_H
#define VALIDATE_H

bool is_valid_count_args( int const count );

bool validate_angle( char* const angle, angle_of_rotation* const result_angle );

valid_status validate_cmd_args( int const count, char** const args,
  char** const source_img, char** const target_img, angle_of_rotation* const angle );

#endif /* VALIDATE_H */
