#include "image.h"
#include "status.h"
#include <stdint.h>
#include <stdio.h>

#ifndef BMP_H
#define BMP_H

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

read_status from_bmp( FILE* source, struct image* img );

write_status to_bmp( FILE* target, struct image const* img );

#endif /* BMP_H */
